package com.openclassrooms.tourguide.user.DTO;

import lombok.Data;

@Data
public class LocationDto {

    private double longitude;
    private double latitude;
}
